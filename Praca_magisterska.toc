\babel@toc {polish}{}
\contentsline {section}{\numberline {1}Wprowadzenie}{5}
\contentsline {section}{\numberline {2}Podobne oprogramowanie}{6}
\contentsline {subsection}{\numberline {2.1}Synaptic}{6}
\contentsline {subsection}{\numberline {2.2}Gconf Cleaner}{7}
\contentsline {subsection}{\numberline {2.3}Cruft}{7}
\contentsline {subsection}{\numberline {2.4}GtkOrphan}{8}
\contentsline {subsection}{\numberline {2.5}Sweeper}{8}
\contentsline {subsection}{\numberline {2.6}BleachBit}{9}
\contentsline {subsection}{\numberline {2.7}Stacer}{11}
\contentsline {section}{\numberline {3}Filesystem Cleaner}{14}
\contentsline {subsection}{\numberline {3.1}Zarys dzia\IeC {\l }ania}{14}
\contentsline {subsection}{\numberline {3.2}Konfiguracja}{14}
\contentsline {subsubsection}{\numberline {3.2.1}Opcje w wierszu polece\IeC {\'n}}{14}
\contentsline {subsubsection}{\numberline {3.2.2}Plik konfiguracyjny}{17}
\contentsline {subsubsection}{\numberline {3.2.3}Eksport ustawie\IeC {\'n}}{18}
\contentsline {subsection}{\numberline {3.3}Najwa\IeC {\.z}niejsze struktury danych}{19}
\contentsline {subsection}{\numberline {3.4}Dzia\IeC {\l }anie}{23}
\contentsline {subsubsection}{\numberline {3.4.1}Skanowanie}{23}
\contentsline {subsubsection}{\numberline {3.4.2}Montowanie}{24}
\contentsline {subsubsection}{\numberline {3.4.3}Interakcja}{25}
\contentsline {subsubsection}{\numberline {3.4.4}W\IeC {\l }a\IeC {\'s}ciwe wykonanie operacji}{26}
\contentsline {section}{\numberline {4}Filtry}{26}
\contentsline {subsection}{\numberline {4.1}Pliki puste}{27}
\contentsline {subsection}{\numberline {4.2}Pliki rzadkie}{27}
\contentsline {subsection}{\numberline {4.3}Duplikaty}{28}
\contentsline {subsection}{\numberline {4.4}Kategoryzacja}{29}
\contentsline {subsection}{\numberline {4.5}W\IeC {\l }asne filtry}{30}
\contentsline {section}{\numberline {5}Biblioteki}{31}
\contentsline {subsection}{\numberline {5.1}Gengetopt}{31}
\contentsline {subsection}{\numberline {5.2}FUSE}{33}
\contentsline {subsection}{\numberline {5.3}JSON}{35}
\contentsline {subsubsection}{\numberline {5.3.1}cJSON}{36}
\contentsline {subsubsection}{\numberline {5.3.2}Frozen}{37}
\contentsline {subsubsection}{\numberline {5.3.3}Jansson}{38}
\contentsline {subsubsection}{\numberline {5.3.4}JSON-C}{40}
\contentsline {subsubsection}{\numberline {5.3.5}Json-parser}{41}
\contentsline {subsubsection}{\numberline {5.3.6}Microjson}{42}
\contentsline {subsubsection}{\numberline {5.3.7}Mjson}{44}
\contentsline {subsubsection}{\numberline {5.3.8}NXJSON}{45}
\contentsline {subsubsection}{\numberline {5.3.9}OjC}{46}
\contentsline {subsubsection}{\numberline {5.3.10}Parson}{47}
\contentsline {subsubsection}{\numberline {5.3.11}YAJL}{49}
\contentsline {subsubsection}{\numberline {5.3.12}Wyb\IeC {\'o}r}{50}
\contentsline {subsection}{\numberline {5.4}OpenSSL}{50}
\contentsline {subsection}{\numberline {5.5}Magic}{52}
\contentsline {section}{\numberline {6}Podsumowanie}{53}
