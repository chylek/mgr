\select@language {polish}
\contentsline {section}{\numberline {1}Wprowadzenie}{7}
\contentsline {subsection}{\numberline {1.1}Historia kostki Rubika}{7}
\contentsline {subsection}{\numberline {1.2}Podobne projekty}{8}
\contentsline {subsubsection}{\numberline {1.2.1}Puzzle Facade}{8}
\contentsline {subsubsection}{\numberline {1.2.2}Rubikon}{9}
\contentsline {section}{\numberline {2}Budowa urz\IeC {\k a}dzenia}{10}
\contentsline {subsection}{\numberline {2.1}Enkodery obrotowe}{11}
\contentsline {subsection}{\numberline {2.2}\IeC {\.Z}yroskop tr\IeC {\'o}josiowy}{13}
\contentsline {subsection}{\numberline {2.3}Mikrokontroler ATmega328p}{14}
\contentsline {subsection}{\numberline {2.4}Chip bluetooth HC-06}{16}
\contentsline {subsection}{\numberline {2.5}P\IeC {\l }ytka drukowana}{17}
\contentsline {subsection}{\numberline {2.6}Zasilanie}{19}
\contentsline {subsection}{\numberline {2.7}Wybrany model kostki}{20}
\contentsline {subsection}{\numberline {2.8}Elementy w\IeC {\l }asne}{22}
\contentsline {section}{\numberline {3}Oprogramowanie}{24}
\contentsline {subsection}{\numberline {3.1}Mikrokontroler}{24}
\contentsline {subsection}{\numberline {3.2}Komunikacja z komputerem}{25}
\contentsline {subsection}{\numberline {3.3}Enkodery obrotowe}{26}
\contentsline {subsection}{\numberline {3.4}\IeC {\.Z}yroskop tr\IeC {\'o}josiowy}{28}
\contentsline {section}{\numberline {4}Podsumowanie}{29}
